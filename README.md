# This is me

An Android demo app to showcase some basic Android development skills with Kotlin programming language.

This app demonstrates the following views and techniques:
* LinearLayout
* TextView
* EditText
* ImageView
* ScrollView
* Use of data binding to improve performance and eliminate findViewById