package com.notmythingy.thisisme

import android.graphics.Color
import android.os.Bundle
import android.text.method.LinkMovementMethod
import android.widget.Button
import android.widget.EditText
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import com.notmythingy.thisisme.databinding.ActivityMainBinding

class MainActivity : AppCompatActivity() {

    private lateinit var binding: ActivityMainBinding

    private val myName: MyName = MyName("Miska Janhunen")

    private lateinit var button: Button
    private var sendto: String = "miska.janhunen@pm.me"
    private var subject: String = "We want you!!!"
    private lateinit var body: EditText

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        //setContentView(R.layout.activity_main)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_main)
        binding.myName = myName

        setupHyperlinks()


        /*button.setOnClickListener {
            body = findViewById(R.id.editText3)
            button = findViewById(R.id.button)

            val emailsend = sendto.getText().toString()
            val emailsubject = subject.getText().toString()
            val emailbody = body.getText().toString()

            // define Intent object with action attribute as ACTION_SEND
            val intent = Intent(Intent.ACTION_SEND)

            // add three fields to intent using putExtra function
            intent.putExtra(Intent.EXTRA_EMAIL, arrayOf(emailsend))
            intent.putExtra(Intent.EXTRA_SUBJECT, emailsubject)
            intent.putExtra(Intent.EXTRA_TEXT, emailbody)

            // set type of intent
            intent.type = "message/rfc822"

            // startActivity with intent with chooser as Email client using createChooser function
            startActivity(Intent.createChooser(intent, "Choose an Email client :"))*/
        }

    private fun setupHyperlinks() {
        val linkTextView = binding.aboutText
        linkTextView.movementMethod = LinkMovementMethod.getInstance()
        linkTextView.setLinkTextColor(Color.BLUE)
    }
}